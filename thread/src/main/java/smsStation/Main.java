package smsStation;

public class Main {
    public static void main(String[] args) {

       SmsStation station = new SmsStation();

       station.addPhone("123456123");
       station.addPhone("456");
       station.addPhone("123");
       station.addPhone("3547");
       station.addPhone("1569");
       station.addPhone("78955");
       station.addPhone("4568");

       station.sendSms("124555423","test");
       station.sendSms("78955","test1");
       station.sendSms("3547","test2");
       station.sendSms("4568","test3");

    }
}
