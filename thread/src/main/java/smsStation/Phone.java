package smsStation;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Phone(String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            Message request = (Message) arg;
            // jeśli sms z requestu jest taki jak telefonu (czyli ten sms jest do mnie)
            if (request.getNumer().equalsIgnoreCase(getPhoneNumber())) {
                System.out.println(phoneNumber +" -> Otrzymalem sms: " + request.getTresc());
            }
        }
    }
}
