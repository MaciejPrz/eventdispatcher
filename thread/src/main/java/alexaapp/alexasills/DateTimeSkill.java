package alexaapp.alexasills;

import alexaapp.alexasills.AbstractSkill;

import java.awt.*;
import java.time.LocalDateTime;

public class DateTimeSkill extends AbstractSkill {
    public DateTimeSkill(){

    }

    @Override
    public void run() {
        System.out.println("Alexa: The time is ->" + LocalDateTime.now());

    }

    @Override
    public void invokeUsing(SystemColor info) {

    }
}
