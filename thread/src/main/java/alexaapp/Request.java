package alexaapp;

public class Request {
    private String request;
    private AlexaDevice deviceToCall;


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public AlexaDevice getAlexaDevice() {
        return deviceToCall;
    }

    public void setAlexaDevice(AlexaDevice deviceToCall) {
        this.deviceToCall = deviceToCall;
    }

    public Request(String request, AlexaDevice deviceToCall){
        this.deviceToCall = deviceToCall;
        this.request =request;

    }
}
