package alexaapp;

import alexaapp.alexasills.AbstractSkill;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.awt.SystemColor.info;

public class AlexaDevice {
    private static int alexaIds=0;
    private int id=alexaIds;



    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    public void sendRequest(String requestString) {

    if(requestString.toLowerCase().startsWith("alexa")){
        Request request = new Request(requestString, this);
        AlexaServer.instance.parseReqest(request);

    }

    }

    public void invoke(AbstractSkill callback) {
        callback.invokeUsing(info);
        executorService.submit(callback);


    }
}
