package alexaapp;

import alexaapp.alexasills.AbstractSkill;
import alexaapp.alexasills.DateTimeSkill;
import alexaapp.alexasills.DontKnowSkill;

public class AlexaServer {
    public static final AlexaServer instance = new AlexaServer();

    private static AlexaServer getInstance() {
        return instance;
    }


    public void update(AlexaDevice alexaDevice, Request request) {
        AbstractSkill callback = parseReqest(request);
        request.getAlexaDevice().invoke(callback);

    }



    public AbstractSkill parseReqest(Request request) {
        String req = request.getRequest();
        req = req.toLowerCase();
        if(req.startsWith("alexa")){
            req = req.substring(6); // 5 znakow oraz
            if (req.toLowerCase().startsWith("what's the time") ||

                    req.toLowerCase().startsWith("give me the time") ||

                    req.toLowerCase().startsWith("the time") ||

                    req.toLowerCase().startsWith("what time is it")) {
                return new DateTimeSkill();
                        }
                    }
                    return new DontKnowSkill();
            }
        }





