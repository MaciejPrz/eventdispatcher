import events.IEvent;
import listener.ICallStartedListener;

import java.util.List;

public class CallStartedEvent implements IEvent {

    private int call_id;

    public CallStartedEvent(int call_id){
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStartedListener> listenersList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallStartedListener.class);
        for (ICallStartedListener listener: listenersList){
            listener.callStarted(call_id);
            
        }

    }
}
