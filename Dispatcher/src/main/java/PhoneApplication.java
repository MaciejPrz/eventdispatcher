import listener.ICallEndedListener;
import listener.ICallStartedListener;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class PhoneApplication  implements ICallStartedListener, ICallEndedListener {


    public PhoneApplication(){
        EventDispatcher.instance.registerObject(this);

    }

    private List<CallEntry> rejest = new LinkedList<>();

    @Override
    public void callStarted(int call_id) {
        rejest.add(new CallEntry(call_id, LocalDateTime.now()));
        System.out.println("PHONE: call_start " + call_id);



    }

    @Override
    public void outgoingCallStarted(int call_id) {
        rejest.add(new CallEntry(call_id, LocalDateTime.now(), CallType.OUTGOIN));
        System.out.println("PHONE: out_call" + call_id);
    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("PHONE: call_ended " +call_id);
        for (CallEntry entry: rejest){
            if(entry.getCall_id()==call_id){
                entry.setTime_ended(LocalDateTime.now());
            }
        }

    }

}
