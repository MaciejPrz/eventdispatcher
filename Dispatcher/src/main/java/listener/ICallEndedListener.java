package listener;

public interface ICallEndedListener {
    void callEnded(int callId);
}
