package listener;

public interface ICallStartedListener {
    void callStarted(int callId);
    void outgoingCallStarted ( int callId);
}
