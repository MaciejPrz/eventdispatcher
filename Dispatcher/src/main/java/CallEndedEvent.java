import events.IEvent;
import listener.ICallEndedListener;

import java.util.List;

public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallEndedListener> listenerList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallEndedListener.class);
        for (ICallEndedListener listener: listenerList){
        listener.callEnded(call_id);
        }

    }
}
