import events.IEvent;
import listener.ICallStartedListener;

import java.util.List;

public class OutgoingCallEvent implements IEvent {

    private int call_id;

    public OutgoingCallEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStartedListener> listenersList = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallStartedListener.class);
        for (ICallStartedListener listener: listenersList) {
            listener.outgoingCallStarted(call_id);
        }
    }
}
