import java.time.LocalDateTime;

public class CallEntry {
    private int call_id;
    private LocalDateTime time_started;
    private LocalDateTime time_ended;
    private CallType type;

    public int getCall_id() {
        return call_id;
    }

    public void setCall_id(int call_id) {
        this.call_id = call_id;
    }

    public LocalDateTime getTime_started() {
        return time_started;
    }

    public void setTime_started(LocalDateTime time_started) {
        this.time_started = time_started;
    }
    public void setTime_ended(LocalDateTime time_ended){
        this.time_ended = time_ended;
    }

    public CallType getType() {
        return type;
    }

    public void setType(CallType type) {
        this.type = type;
    }

    public CallEntry(int call_id, LocalDateTime time_started, CallType type) {
        this.call_id = call_id;
        this.time_started = time_started;
        this.type = type;
    }
    public CallEntry(int call_id, LocalDateTime time_started){
        this.call_id = call_id;
        this.time_started = time_started;
        this.type = CallType.INCOMING;
    }
}

