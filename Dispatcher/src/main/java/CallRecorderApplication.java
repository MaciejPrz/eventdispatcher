import listener.ICallEndedListener;
import listener.ICallStartedListener;

public class CallRecorderApplication implements ICallStartedListener, ICallEndedListener {
    public CallRecorderApplication(){
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int call_id) {
        System.out.println("CALER: call_start " + call_id);

    }

    @Override
    public void outgoingCallStarted(int callId) {
        System.out.println("Caller: out_call_started " + callId );

    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("Caller: call_ended "+ call_id);

    }
}
