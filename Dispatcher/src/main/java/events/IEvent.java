package events;

public interface IEvent {
    void run();
}
